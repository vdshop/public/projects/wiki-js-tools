<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Enum\WikiPage;

/**
 * Enum TagKeys.
 *
 * Allowed tag keys.
 */
enum TagKeys: string
{
    case SECTION = 'section';
    case CATEGORY = 'category';
    case TOPIC = 'topic';
    case PAGE_TYPE = 'page type';
}
