<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Enum\WikiPage;

/**
 * Enum MetadataKeys.
 *
 * Known WikiPage metadata keys.
 */
enum MetadataKeys: string
{
    case TITLE = 'title';
    case DESCRIPTION = 'description';
    case PUBLISHED = 'published';
    case DATE = 'date';
    case TAGS = 'tags';
    case EDITOR = 'editor';
    case DATE_CREATED = 'dateCreated';
}
