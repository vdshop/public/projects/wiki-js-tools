<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Provider\Filesystem;

use CallbackFilterIterator;
use Closure;
use FilesystemIterator;
use SplFileInfo;

/**
 * Class Iterator.
 *
 * Provide iterators and SplFileInfo arrays.
 */
class Iterator
{
    /**
     * Get iterator from path, with optional callback filter.
     * Path must be a directory.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return \Iterator
     */
    public function getIterator(
        string   $path,
        ?Closure $callbackFilter = null
    ): \Iterator {
        return null === $callbackFilter
            ? new FilesystemIterator(directory: $path)
            : new CallbackFilterIterator(
                iterator: $this->getIterator(path: $path),
                callback: fn(SplFileInfo $fileInfo): bool => $callbackFilter($fileInfo)
            );
    }

    /**
     * Get SplFileInfo array from path, with optional callback filter.
     * Path must be a directory.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return SplFileInfo[]
     */
    public function getIteratorArray(
        string   $path,
        ?Closure $callbackFilter = null
    ): array {
        $iteratorArray = \iterator_to_array(
            iterator:      $this->getIterator(path: $path, callbackFilter: $callbackFilter),
            preserve_keys: true,
        );

        \ksort(array: $iteratorArray);

        return $iteratorArray;
    }

    /**
     * Get files iterator.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return \Iterator
     */
    public function getFilesIterator(
        string $path,
        ?Closure $callbackFilter = null
    ): \Iterator {
        return $this->getIterator(
            path: $path,
            callbackFilter: fn(SplFileInfo $fileInfo): bool =>
                $fileInfo->isFile() && (null === $callbackFilter || $callbackFilter($fileInfo)),
        );
    }

    /**
     * Get files array.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return SplFileInfo[]
     */
    public function getFilesArray(
        string $path,
        ?Closure $callbackFilter = null
    ): array {
        return $this->iteratorToArray(
            iterator: $this->getFilesIterator(path: $path, callbackFilter: $callbackFilter),
        );
    }

    /**
     * Get directories iterator.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return \Iterator
     */
    public function getDirsIterator(
        string $path,
        ?Closure $callbackFilter = null
    ): \Iterator {
        return $this->getIterator(
            path: $path,
            callbackFilter: fn(SplFileInfo $fileInfo): bool =>
                $fileInfo->isDir() && (null === $callbackFilter || $callbackFilter($fileInfo)),
        );
    }

    /**
     * Get directories array.
     *
     * @param string       $path
     * @param Closure|null $callbackFilter
     *
     * @return SplFileInfo[]
     */
    public function getDirsArray(
        string $path,
        ?Closure $callbackFilter = null
    ): array {
        return $this->iteratorToArray(
            iterator: $this->getDirsIterator(path: $path, callbackFilter: $callbackFilter),
        );
    }

    /**
     * Convert iterator to array.
     *
     * @param \Iterator $iterator
     *
     * @return SplFileInfo[]
     */
    private function iteratorToArray(\Iterator $iterator): array
    {
        $iteratorArray = \iterator_to_array(
            iterator:      $iterator,
            preserve_keys: true,
        );

        \ksort(array: $iteratorArray);

        return $iteratorArray;
    }
}
