<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata;

/**
 * Interface MetadataFactory.
 *
 * WikiPage model metadata factory.
 */
interface MetadataFactory
{
    /**
     * Create WikiPage metadata object.
     *
     * @param string|Stringable $metadata
     */
    public function create(string|Stringable $metadata): Metadata;
}
