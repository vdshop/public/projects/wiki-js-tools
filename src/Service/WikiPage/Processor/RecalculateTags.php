<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Enum\Content\PageTypes as PageTypesEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys as TagKeysEnum;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class RecalculateTags.
 *
 * Calculate and replace tags.
 */
class RecalculateTags implements WikiPageProcessor
{
    use GetRelativePath;
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'RECALCULATE_TAGS';

    /**
     * RecalculateTags constructor.
     *
     * @param FeatureFlag $featureFlag
     * @param Logger      $logger
     */
    public function __construct(
        private readonly FeatureFlag $featureFlag,
        private readonly Logger $logger,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPage $wikiPage): void
    {
        if (!$this->supports(wikiPage: $wikiPage)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $wikiPage->getFileInfo()->getType() .
                         ' ' .
                         $wikiPage->getFileInfo()->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $origTags = (string)$wikiPage->getMetadata()->getTags();
        $relativePath = \trim(
            string:     \str_replace(
                            search:  CONTENT_PATH,
                            replace: '',
                            subject: $wikiPage->getFileInfo()->getPath()
                        ),
            characters: DIRECTORY_SEPARATOR,
        );

        $pathParts = \explode(
            separator: DIRECTORY_SEPARATOR,
            string:    $relativePath,
        );

        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::SECTION,
            tagValues: \array_shift(array: $pathParts),
        );

        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::CATEGORY,
            tagValues: $pathParts,
        );

        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::TOPIC,
            tagValues: $wikiPage->getFileInfo()->getBasename(suffix: '.' . WikiPage::FILE_EXTENSION),
        );

        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::PAGE_TYPE,
            tagValues: PageTypesEnum::CONTENT->value,
        );

        if (\is_dir(
            filename: $wikiPage->getFileInfo()->getPath() .
                      DIRECTORY_SEPARATOR .
                      $wikiPage->getFileInfo()->getBasename(suffix: '.' . WikiPage::FILE_EXTENSION)
        )) {
            $wikiPage->getMetadata()->getTags()->setByKey(
                tagKey:    TagKeysEnum::PAGE_TYPE,
                tagValues: PageTypesEnum::INDEX->value,
            );
        }

        $finalTags = (string)$wikiPage->getMetadata()->getTags();

        if ($origTags !== $finalTags) {
            $this->logger->notice(message: '[!] Updated tags:');
            $this->logger->increaseIndentation();
            $this->logger->notice(message: 'from: ' . $origTags);
            $this->logger->notice(message: 'to  : ' . $finalTags);
            $this->logger->decreaseIndentation();
        }
    }

    /**
     * Check if this processor supports given wiki page.
     *
     * @param WikiPage $wikiPage
     *
     * @return bool
     */
    private function supports(WikiPage $wikiPage): bool
    {
        $fileInfo = $wikiPage->getFileInfo();

        return $fileInfo->isFile() && $fileInfo->getExtension() === WikiPage::FILE_EXTENSION;
    }
}
