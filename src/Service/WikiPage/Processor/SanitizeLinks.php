<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor;

use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class SanitizeLinks.
 *
 * Sanitize links.
 */
class SanitizeLinks implements WikiPageProcessor
{
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'SANITIZE_LINKS';

    /**
     * SanitizeLinks constructor.
     *
     * @param FeatureFlag $featureFlag
     * @param Logger      $logger
     */
    public function __construct(
        private readonly FeatureFlag $featureFlag,
        private readonly Logger $logger
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(WikiPage $wikiPage): void
    {
        if (!$this->supports(wikiPage: $wikiPage)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $wikiPage->getFileInfo()->getType() .
                         ' ' .
                         $wikiPage->getFileInfo()->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $origContent = $wikiPage->getContent()->get();

        $processedContent = \preg_replace(
            pattern:     '#\[(.*)]\(\*+([^*]*)\)#U',
            replacement: '[$1]($2)',
            subject:     $origContent,
        );

        if ($origContent !== $processedContent) {
            $wikiPage->getContent()->set($processedContent);
            $this->logger->notice(message: '[!] ' . self::FF_NAME . ' updated content');
            $this->logger->increaseIndentation();
            $this->logger->debug(message: 'from: ' . $origContent);
            $this->logger->debug(message: 'to  : ' . $processedContent);
            $this->logger->decreaseIndentation();
        }
    }

    /**
     * Check if this processor supports given wiki page.
     *
     * @param WikiPage $wikiPage
     *
     * @return bool
     */
    private function supports(WikiPage $wikiPage): bool
    {
        return $wikiPage->getFileInfo()->isFile();
    }
}
