<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor\Directives;

use Stringable;
use Vdshop\WikiJsTools\Contract\Directive\Processor as DirectiveProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;

/**
 * Class Redirect.
 *
 * Process redirect directives.
 */
class Redirect implements DirectiveProcessor
{
    /**
     * @inheritdoc
     */
    public function execute(array $directiveArgs, WikiPage $wikiPage): string|Stringable
    {
        if ([] === $directiveArgs) {
            return '';
        }

        $targetUrl = (string)\reset(array: $directiveArgs);

        if ('' === $targetUrl) {
            return '';
        }

        return <<<"EOD"
            > You are being redirected, please wait...
            {.is-info}
            
            <br>
             
            <img src="/assets/images/redirect/301.png"
              class="elevation-5 radius-5"
              onload="window.location.href='$targetUrl'"
              />
            EOD;

    }
}
