<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Processor\Directives;

use Stringable;
use Vdshop\WikiJsTools\Contract\Directive\Processor as DirectiveProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;

/**
 * Class Includes.
 *
 * Process include directives.
 */
class Includes implements DirectiveProcessor
{
    /**
     * @inheritdoc
     */
    public function execute(array $directiveArgs, WikiPage $wikiPage): string|Stringable
    {
        $includePath = \reset(array: $directiveArgs);

        if (!\is_string($includePath) || '' === $includePath) {
            return '';
        }

        return $this->getIncludedContentDisclaimer(includePath: $includePath) .
            (\file_get_contents(filename: $includePath) ?: '');
    }

    /**
     * Give credits to included content when it comes from an existing url.
     *
     * @param string $includePath
     *
     * @return string
     */
    private function getIncludedContentDisclaimer(string $includePath): string
    {
        return false !== \filter_var(value: $includePath, filter: FILTER_VALIDATE_URL)
            ? "> Content adapted from $includePath" . PHP_EOL . '{.is-info}' . PHP_EOL . PHP_EOL : '';
    }
}
