<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\WikiPage\Metadata;

use Stringable;
use Vdshop\WikiJsTools\Model\WikiPage\Metadata\Description;

/**
 * Interface DescriptionFactory.
 *
 * WikiPage model description factory.
 */
interface DescriptionFactory
{
    /**
     * Create WikiPage description object.
     */
    public function create(string|Stringable $description): Description;
}
