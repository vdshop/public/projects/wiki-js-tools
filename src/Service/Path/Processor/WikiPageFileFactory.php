<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\Path\Processor\WikiPageFile;

/**
 * Interface WikiPageFileFactory.
 */
interface WikiPageFileFactory
{
    /**
     * Create WikiPageFile object.
     */
    public function create(WikiPageProcessor $wikiPageProcessor): WikiPageFile;
}
