<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;

/**
 * Class Log.
 *
 * Print path being processed.
 */
class Log implements PathProcessor
{
    use GetRelativePath;

    /**
     * Log constructor.
     *
     * @param Logger      $logger
     */
    public function __construct(
        private readonly Logger $logger,
    ) {
    }

    /**
     * Print path being processed.
     *
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            $this->logger->debug(message: \get_class($this) . ' does not support path ' . $fileInfo->getRealPath());
            return;
        }

        $relativePath = $this->getRelativePath(absolutePath: $fileInfo->getRealPath()) ?: 'root directory';

        $this->logger->info(message: 'Processing ' . $fileInfo->getType() . ': ' . $relativePath);
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        return \str_starts_with(
            haystack: $fileInfo->getRealPath(),
            needle: CONTENT_PATH,
        );
    }
}
