<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Model\Path\Processor\Recursive;

/**
 * Interface RecursiveFactory.
 */
interface RecursiveFactory
{
    /**
     * Create Recursive object.
     */
    public function create(
        PathProcessor $pathProcessor,
        bool $childrenFirst = false,
        bool $filesFirst = false,
    ): Recursive;
}
