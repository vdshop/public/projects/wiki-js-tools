<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Service\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Enum\Content\PageTypes as PageTypesEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\Editor as EditorEnum;
use Vdshop\WikiJsTools\Enum\WikiPage\TagKeys as TagKeysEnum;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\FeatureFlag;
use Vdshop\WikiJsTools\Service\Provider\Filesystem\Iterator;
use Vdshop\WikiJsTools\Service\WikiPage\Processor\Directives;
use Vdshop\WikiJsTools\Service\WikiPage\Repository as WikiPageRepository;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;
use Vdshop\WikiJsTools\Traits\Path\IsFeatureFlagEnabled;

/**
 * Class EnsureDirectoryIndexExists.
 *
 * Class name is self-descriptive.
 */
class EnsureDirectoryIndexExists implements PathProcessor
{
    use GetRelativePath;
    use IsFeatureFlagEnabled;

    private const FF_NAME = 'ENSURE_DIRECTORY_INDEX_EXISTS';

    /**
     * EnsureDirectoryIndexExists constructor.
     *
     * @param WikiPageRepository $wikiPageRepository
     * @param FeatureFlag        $featureFlag
     * @param Iterator           $filesystemIterator
     * @param Logger             $logger
     */
    public function __construct(
        private readonly WikiPageRepository $wikiPageRepository,
        private readonly FeatureFlag $featureFlag,
        private readonly Iterator $filesystemIterator,
        private readonly Logger $logger,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' does not support ' .
                         $fileInfo->getType() .
                         ' ' .
                         $fileInfo->getRealPath()
            );

            return;
        }

        if (!$this->isFeatureFlagEnabled()) {
            $this->logger->debug(
                message: \get_class($this) .
                         ' skipped, feature flag ' .
                         FeatureFlag::DEFAULT_PREFIX .
                         self::FF_NAME .
                         ' not enabled.'
            );

            return;
        }

        $filePath = $this->getFileRealPath(fileInfo: $fileInfo);
        $fileRelativePath = $this->getRelativePath(absolutePath: $filePath);

        \touch(filename: $filePath);

        $wikiPage = $this->wikiPageRepository->get(file: $filePath);

        $wikiPage->getMetadata()->getTitle()->set(
            title: \ucfirst(
                       string: \preg_replace(
                                   pattern:     '#[^a-zA-Z0-9]#',
                                   replacement: ' ',
                                   subject:     $wikiPage->getFileInfo()->getBasename(
                                                    suffix: '.' . WikiPage::FILE_EXTENSION
                                                )
                               )
                   )
        );
        $wikiPage->getMetadata()->getDescription()->set(description: $fileRelativePath . ' directory index.');
        $wikiPage->getMetadata()->getTags()->setByKey(
            tagKey:    TagKeysEnum::PAGE_TYPE,
            tagValues: PageTypesEnum::INDEX->value
        );
        $wikiPage->getMetadata()->getPublished()->set(published: true);
        $wikiPage->getMetadata()->getEditor()->set(editor: EditorEnum::MARKDOWN);
        $wikiPage->getContent()->set(
            content: Directives::DIRECTIVE_OPEN . 'directory-index' . Directives::DIRECTIVE_CLOSE
        );

        $this->wikiPageRepository->save(wikiPage: $wikiPage);

        $this->logger->notice(
            message: '[!] Created directory index: ' . $this->getRelativePath(absolutePath: $filePath)
        );
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        return $fileInfo->isDir() &&
            CONTENT_PATH !== $fileInfo->getRealPath() &&
            !\file_exists(filename: $this->getFileRealPath(fileInfo: $fileInfo)) &&
            0 < \count(
                value: $this->filesystemIterator->getFilesArray(
                         $fileInfo->getRealPath(),
                         static fn(SplFileInfo $currentFileInfo): bool => $currentFileInfo->getExtension() ===
                             WikiPage::FILE_EXTENSION
                     )
            );
    }

    /**
     * Get expected directory index file real path.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return string
     */
    private function getFileRealPath(SplFileInfo $fileInfo): string
    {
        return $fileInfo->getRealPath() . '.' . WikiPage::FILE_EXTENSION;
    }
}
