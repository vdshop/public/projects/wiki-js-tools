<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Contract\WikiPage;

use Vdshop\WikiJsTools\Exception\ProcessorException;
use Vdshop\WikiJsTools\Model\WikiPage as WikiPageModel;

/**
 * Interface Processor.
 *
 * Perform some kind of processing on a WikiPage object.
 */
interface Processor
{
    /**
     * Perform actual WikiPage object processing.
     *
     * @throws ProcessorException
     */
    public function execute(WikiPageModel $wikiPage): void;
}
