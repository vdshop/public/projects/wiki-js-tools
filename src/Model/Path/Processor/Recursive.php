<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Logger;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Service\Path\Processor\Log as LogPathProcessor;
use Vdshop\WikiJsTools\Service\Provider\Filesystem\Iterator as IteratorProvider;
use Vdshop\WikiJsTools\Traits\Path\GetRelativePath;

/**
 * Class Recursive.
 *
 * Apply supplied path processor recursively.
 */
class Recursive implements PathProcessor
{
    use GetRelativePath;

    /**
     * Recursive constructor.
     *
     * @param Logger           $logger
     * @param LogPathProcessor $logPathProcessor
     * @param IteratorProvider $iteratorProvider
     * @param PathProcessor    $pathProcessor
     * @param bool             $childrenFirst
     * @param bool             $filesFirst
     */
    public function __construct(
        private readonly Logger $logger,
        private readonly LogPathProcessor $logPathProcessor,
        private readonly IteratorProvider $iteratorProvider,
        private readonly PathProcessor $pathProcessor,
        private readonly bool $childrenFirst = false,
        private readonly bool $filesFirst = false,
    ) {
    }

    /**
     * Apply supplied path processor recursively.
     *
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            return;
        }

        $this->childrenFirst
            ? $this->childrenFirst(fileInfo: $fileInfo)
            : $this->selfFirst(fileInfo: $fileInfo);
    }

    /**
     * Process children first.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function childrenFirst(SplFileInfo $fileInfo): void
    {
        $this->processChildrenDirs(fileInfo: $fileInfo);
        $this->processCurrentDir(fileInfo: $fileInfo);
    }

    /**
     * Process self first.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function selfFirst(SplFileInfo $fileInfo): void
    {
        $this->processCurrentDir(fileInfo: $fileInfo);
        $this->processChildrenDirs(fileInfo: $fileInfo);
    }

    /**
     * Process current directory.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function processCurrentDir(SplFileInfo $fileInfo): void
    {
        if ($this->filesFirst) {
            $this->processCurrentDirFiles(fileInfo: $fileInfo);
            $this->processCurrentFileInfo(fileInfo: $fileInfo);
        }

        if (!$this->filesFirst) {
            $this->processCurrentFileInfo(fileInfo: $fileInfo);
            $this->processCurrentDirFiles(fileInfo: $fileInfo);
        }
    }

    /**
     * Process current file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function processCurrentFileInfo(SplFileInfo $fileInfo): void
    {
        $this->logPathProcessor->execute(fileInfo: $fileInfo);
        $this->logger->increaseIndentation();
        try {
            $fileInfo->getType();
            $this->pathProcessor->execute(fileInfo: $fileInfo);
        } catch (\RuntimeException) {
            $this->logger->warning('Unable to determine file info type, it may not exist anymore, resuming...');
        }

        $this->logger->decreaseIndentation();
        $this->logger->debug('|');
    }

    /**
     * Process current directory files.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function processCurrentDirFiles(SplFileInfo $fileInfo): void
    {
        \array_map(
            array: $this->iteratorProvider->getFilesArray(
                path: $fileInfo->getRealPath(),
                callbackFilter: $this->filterFileInfo(...)
            ),
            callback: fn(SplFileInfo $fileFileInfo) => $this->processCurrentFileInfo($fileFileInfo),
        );
    }

    /**
     * Traverse and process children directories.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return void
     */
    private function processChildrenDirs(SplFileInfo $fileInfo): void
    {
        $this->logger->increaseIndentation();

        \array_map(
            array: $this->iteratorProvider->getDirsArray(
                path: $fileInfo->getRealPath(),
                callbackFilter: $this->filterFileInfo(...)
            ),
            callback: fn(SplFileInfo $dirFileInfo) => $this->execute(fileInfo: $dirFileInfo),
        );

        $this->logger->decreaseIndentation();
    }

    /**
     * Check if this path processor supports current path.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        return \str_starts_with(haystack: $fileInfo->getRealPath(), needle: CONTENT_PATH)
            && ($fileInfo->isDir() || $fileInfo->isFile());
    }

    /**
     * Filter file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function filterFileInfo(SplFileInfo $fileInfo): bool
    {
        return \str_starts_with(haystack: $fileInfo->getRealPath(), needle: CONTENT_PATH)
            && !\str_starts_with(haystack: $fileInfo->getRealPath(), needle: CONTENT_PATH . DIRECTORY_SEPARATOR . '.git');
    }
}
