<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\Path\Processor;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Contract\WikiPage\Processor as WikiPageProcessor;
use Vdshop\WikiJsTools\Model\WikiPage;
use Vdshop\WikiJsTools\Service\WikiPage\Repository as WikiPageRepository;

/**
 * Class WikiPageFile.
 *
 * Process WikiPage file using a WikiPage processor.
 */
class WikiPageFile implements PathProcessor
{
    /**
     * WikiPageFile constructor.
     *
     * @param WikiPageRepository $wikiPageRepository
     * @param WikiPageProcessor  $wikiPageProcessor
     */
    public function __construct(
        private readonly WikiPageRepository $wikiPageRepository,
        private readonly WikiPageProcessor $wikiPageProcessor,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function execute(SplFileInfo $fileInfo): void
    {
        if (!$this->supports(fileInfo: $fileInfo)) {
            return;
        }

        $wikiPage = $this->wikiPageRepository->get($fileInfo);
        $this->wikiPageProcessor->execute(wikiPage: $wikiPage);
        $this->wikiPageRepository->save($wikiPage);
    }

    /**
     * Check if this processor supports given file info.
     *
     * @param SplFileInfo $fileInfo
     *
     * @return bool
     */
    private function supports(SplFileInfo $fileInfo): bool
    {
        return $fileInfo->isFile() && $fileInfo->getExtension() === WikiPage::FILE_EXTENSION;
    }
}
