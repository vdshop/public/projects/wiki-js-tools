<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\Project;

use SplFileInfo;
use Vdshop\WikiJsTools\Contract\Path\Processor as PathProcessor;
use Vdshop\WikiJsTools\Contract\Project\Processor as ProjectProcessor;

/**
 * Class Processor.
 *
 * Process project files using supplied path processor.
 */
class Processor implements ProjectProcessor
{
    /**
     * Processor constructor.
     *
     * @param PathProcessor    $pathProcessor
     */
    public function __construct(
        private readonly PathProcessor $pathProcessor,
    ) {
    }

    /**
     * Delegate processing to path processor.
     *
     * @inheritdoc
     */
    public function execute(): void
    {
        $this->pathProcessor->execute(fileInfo: new SplFileInfo(filename: CONTENT_PATH));
    }
}
