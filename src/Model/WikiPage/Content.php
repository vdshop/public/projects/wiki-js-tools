<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage;

use Stringable;

/**
 * Class Content.
 *
 * WikiPage actual content.
 */
class Content implements Stringable
{
    /**
     * Content constructor.
     *
     * @param string|Stringable $content
     */
    public function __construct(
        private string|Stringable $content,
    ) {
        $this->set(content: $content);
    }

    /**
     * Get actual page content.
     *
     * @return string|Stringable
     */
    public function get(): Stringable|string
    {
        return $this->content;
    }

    /**
     * Set actual page content.
     *
     * @param string|Stringable $content
     */
    public function set(Stringable|string $content): void
    {
        $this->content = $content;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
}
