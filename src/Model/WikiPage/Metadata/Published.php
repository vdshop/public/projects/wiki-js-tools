<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use Stringable;

/**
 * Class Published.
 *
 * WikiPage published.
 */
class Published implements Stringable
{
    public const PUBLISHED = 'true';
    public const UNPUBLISHED = 'false';

    /**
     * @var bool
     */
    private bool $published;

    /**
     * Published constructor.
     *
     * @param string|Stringable|bool $published
     */
    public function __construct(
        string|Stringable|bool $published = true,
    ) {
        $this->set(published: $published);
    }

    /**
     * Get published.
     *
     * @return string
     */
    public function get(): string
    {
        return $this->is() ? self::PUBLISHED : self::UNPUBLISHED;
    }

    /**
     * Is published.
     *
     * @return bool
     */
    public function is(): bool
    {
        return $this->published;
    }

    /**
     * Set published.
     *
     * @param string|Stringable|bool $published
     *
     * @return void
     */
    public function set(string|Stringable|bool $published)
    {
        $this->published = \is_bool($published) ? $published : self::PUBLISHED === (string)$published;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
}
