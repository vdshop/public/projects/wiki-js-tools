<?php

/**
 * @author vdSHOP Team
 * @copyright Copyright © vdSHOP (https://vdshop.es/)
 */

declare(strict_types=1);

namespace Vdshop\WikiJsTools\Model\WikiPage\Metadata;

use DateTimeImmutable;
use DateTimeInterface;
use Stringable;

/**
 * Class DateCreated.
 *
 * WikiPage date created.
 */
class DateCreated implements Stringable
{
    public const DATE_FORMAT = Date::DATE_FORMAT;

    /**
     * @var DateTimeImmutable
     */
    private readonly DateTimeImmutable $dateCreated;

    /**
     * DateCreated constructor.
     *
     * @param string|Stringable|DateTimeInterface|null $dateCreated
     */
    public function __construct(
        string|Stringable|DateTimeInterface|null $dateCreated = null,
    ) {
        if (\is_null(value: $dateCreated) || \is_string(value: $dateCreated) || $dateCreated instanceof Stringable) {
            $dateCreated = DateTimeImmutable::createFromFormat(
                datetime: (string)$dateCreated,
                format: self::DATE_FORMAT,
            ) ?: new DateTimeImmutable;
        }

        $this->dateCreated = DateTimeImmutable::createFromInterface(object: $dateCreated);
    }

    /**
     * Get date created as string.
     *
     * @return string|Stringable
     */
    public function getAsString(): string|Stringable
    {
        return $this->getAsDateTime()->format(format: self::DATE_FORMAT);
    }


    /**
     * Get date created as DateTimeImmutable.
     *
     * @return DateTimeImmutable
     */
    public function getAsDateTime(): DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * String representation of current class.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getAsString();
    }
}
